window.addEventListener('load', function(){

    function generateMap(option){
        let create = option;
        let cantBallons = 20;
        for(let i = 0 ; i < cantBallons ; i++){
            let randomColor;
            randomColor = Math.floor(Math.random()*16777215).toString(16);
            if(create === true){
                let container = document.querySelector("#container-ballons");
                let div = document.createElement("div");
                div.setAttribute('class', 'ballons');
                div.setAttribute('style', `background-color: #${randomColor}`);
                container.appendChild(div);
            }else{
                let resetDivs = document.querySelectorAll('.ballons');
                resetDivs[i].classList.remove('used');
                resetDivs[i].setAttribute('style', `background-color: #${randomColor}`);
            }
        }
    }

    function validateMap(){
        let cont = 0;
        for (const ballon of ballons) {
            if(ballon.classList.contains('used')){
                cont++
            }
        }
        if(cont == ballons.length){
            console.log('reset');
            generateMap(false);
        }
    }
    
    //Se inicia la aplicación
    generateMap(true);
    
    var ballons = document.querySelectorAll('.ballons');
    ballons.forEach((item)=>{
        item.addEventListener('click', function(ev){
            ev.preventDefault();
            if(!item.classList.contains('used')){
                item.setAttribute('style', 'background-color: #ffffff; border-color: #ffffff');
                item.classList.add('used')
            }
            validateMap();
        });
    });
});